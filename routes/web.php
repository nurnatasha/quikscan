<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\VisitorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('visitorrs', function () {
    return view('visitorrs');
});

Route::get('visitorr', function () {
    return view('visitorr');
});

Route::get('events', function () {
    return view('events');
});

Route::get('history', function () {
    return view('history');
});

Route::get('historyPreRegister', function () {
    return view('historyPreRegister');
});


Route::get('userEdit', function () {
    return view('userEdit');
});

Route::get('qrCode', function () {
    return view('qrCode');
});

Route::get('qr-code-g', function () {


    \QrCode::size(500)

            ->format('png')

            ->generate('HDTuto.com', public_path('images/qrcode.png'));

  return view('qrCode');

});

Route::get('homeAdmin', function () {
    return view('homeAdmin');
});

Route::get('CheckInVisitor', function () {
    return view('CheckInVisitor');
});

Route::get('RegInVisitor', function () {
    return view('RegInVisitor');
});

Route::get('reg_visitor', function () {
    return view('reg_visitor');
});

Route::get('CheckOutVisitor', function () {
    return view('CheckOutVisitor');
});

Route::get('reg_visitors', function () {
    return view('reg_visitors');
});


Route::get('historyAdmin', function () {
    return view('historyAdmin');
});

Route::get('adminProfile', function () {
    return view('adminProfile');
});

Route::resource('visitorrs','VisitorController');
Route::resource('events','EventController');
Route::resource('reg_visitors','RegVisitorController');
Route::resource('users','UserController');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/homes', 'HomeController@index')->name('homes');

Route::get('admin/home', 'HomeController@adminHome')->name('admin.home')->middleware('is_admin');

Route::get('/search2', 'VisitorController@search2')->name('Search2');

Route::get('/historyPreRegister', 'historyPreRegisterController@index')->name('historyPreRegister');

Route::get('/search3', 'EventController@search3')->name('Search3');

Route::get('/search4', 'CheckOutController@search4')->name('Search4');

Route::get('/CheckOutVisitor', 'CheckOutController@index')->name('CheckOutVisitor');

Route::get('/CheckInVisitor', 'CheckInController@index')->name('CheckInVisitor');

Route::get('/search', 'CheckInController@search')->name('CheckInVisitor');

Route::get('/search5', 'HistoryAdminVisitorController@search5')->name('Search5');

Route::get('/search6', 'RegVisitorController@search6')->name('Search6');

Route::get('/historyVisitor', 'HistoryVisitorController@index')->name('historyVisitor');

Route::get('/search7', 'HistoryVisitorController@search7')->name('Search7');

Route::get('/historyEvent', 'HistoryEventController@index')->name('historyEvent');

Route::get('/search8', 'HistoryEventController@search8')->name('Search8');

Route::get('/CheckOut', 'CheckOut2Controller@index2')->name('CheckOut');

Route::get('/search9', 'CheckOut2Controller@search9')->name('Search9');

Route::get('/edit/{id}', 'UserController@edit')->name('Edit');

Route::post('/update/{id}', 'UserController@update')->name('Update');

Route::get('/userEdit', 'UserController@index')->name('userEdit');

Route::get('users/edit2/{id}', 'UserController@edit2')->name('Edit2');

Route::patch('users/update2/{id}', 'UserController@update2')->name('update2');

Route::get('/adminProfile', 'UserController@adminIndex')->name('adminProfile');

Route::get('/changePassword', 'UserController@changePassword')->name('changePassword');

Route::post('users/updatePassword/', 'UserController@updatePassword')->name('updatePassword');

Route::get('/changePassword2', 'UserController@changePassword2')->name('changePassword2');

Route::post('users/updatePassword2/', 'UserController@updatePassword2')->name('updatePassword2');

//Route::get('/CheckInVisitor/edit2/{id}', 'CheckInController@edit2')->name('CheckInVisitor.edit2');

Route::get('active/{id}', 'CheckInController@active')->name('active');
Route::get('inactive/{id}', 'CheckInController@inactive')->name('inactive');

Route::get('active2/{id}', 'CheckOutController@active2')->name('active2');
Route::get('inactive2/{id}', 'CheckOutController@inactive2')->name('inactive2');

Route::get('active4/{id}', 'CheckOut2Controller@active4')->name('active4');
Route::get('inactive4/{id}', 'CheckOut2Controller@inactive4')->name('inactive4');

Route::get('/displayCheckIn/{token}', 'VisitorController@checkin')->name('displayCheckIn');
Route::post('/visitorCheckedIn', 'VisitorController@checkedin')->name('visitorCheckedIn');

Route::get('/displayCheckOut/{token}', 'VisitorController@checkout')->name('displayCheckOut');
Route::post('/visitorCheckedOut', 'VisitorController@checkedout')->name('visitorCheckedOut');
