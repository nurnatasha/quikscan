@extends('layouts.app')

@section('content')
<div class="container">
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update a visitor</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br />
        @endif
        <form method="post" action="{{ route('events.update', $event->id) }}">
            @method('PATCH')
            @csrf
            <div class="form-group">

                <label for="type_event">Type of event:</label>
                <input type="text" class="form-control" name="type_event" value={{ $event->type_event }} />
            </div>

            <div class="form-group">
                <label for="select_date">Date:</label>
                <input type="text" class="form-control" name="select_date" value={{ $event->select_date }} />
            </div>

            <div class="form-group">
                <label for="select_time">Time:</label>
                <input type="text" class="form-control" name="select_time" value={{ $event->select_time }} />
            </div>
            <div class="form-group">
                <label for="num_visitor">Number of visitor:</label>
                <input type="text" class="form-control" name="num_visitor" value={{ $event->num_visitor }} />
            </div>

            <div>
            <button type="submit" class="btn btn-primary">Update</button>
          </div>
        </form>
    </div>
</div>
@endsection
