@extends('layouts.app')

@section('content')
<head>

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
</head>

<style>
.col-20 {
float: left;
width: 20%;
margin-top: 6px;
}

.col-75 {
float: left;
width: 75%;
margin-top: 6px;
}
</style>

<div class="container">
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3"><center>Register Event</center></h1>
  <div>

      <form method="post" action="{{ route('events.store') }}">
          @csrf
          <div class="col-20">
              <label for="type_event">Type of event:</label>
          </div>
          <div class="col-75">
            <select id="type_event" class="form-control" name="type_event">
                <option value="birthday">Birthday party</option>
                <option value="wedding">Wedding</option>
                <option value="openhouse">Open house</option>
                <option value="other">Other</option>
            </select>
          </div>

          <br>
          </br>

          <div class="col-20">
              <label for="select_date">Date:</label>
          </div>
          <div class="col-75">
              <input type="text" class="date form-control" name="select_date" />
          </div>

          <script type="text/javascript">
            $('.date').datepicker({
                format: 'dd/mm/yyyy'
                });
          </script>

          <br>
          </br>

          <div class="col-20">
              <label for="select_time">Time:</label>
          </div>
          <div class="col-75">
              <input type="text" class="timepicker form-control" name="select_time"/>
          </div>

          <script type="text/javascript">
            $('.timepicker').datetimepicker({
                format: 'HH:mm'
              });
          </script>

          <br>
          </br>

          <div class="col-20">
              <label for="num_visitor">Number of visitor:</label>
          </div>
          <div class="col-75">
            <select id="num_visitor" class="form-control" name="num_visitor">
                <option value="1 to 99">1 - 99</option>
                <option value="100 - 999">100 - 999</option>
                <option value="More than 999">More than 999</option>
            </select>
          </div>

          <br>
          </br>

          <center>
          <button type="submit" class="btn btn-success">Register</button>
          </center>

      </form>
  </div>
</div>
</div>
</div>
@endsection
