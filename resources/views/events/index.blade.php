@extends('layouts.app')

@section('content')
<div class="container">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Events</h1>
    <table class="table table-striped">

      <div class="col-md-5">
        <form action="/search3" method="get">
        <div class="input-group">
          <button type="submit"a href="http://localhost:8000/events"><i class="fa fa-arrow-left" style="font-size:15px;color:black" ></i></a></button>
          <input type="search3" name="search3" class="form-control">
          <span class="input-group-prepend">
            <button type="submit" class="btn btn-primary">Search</button>
          </span>
        </div>
        </form>
      </div>

      <br>

    <thead>
        <tr>
          <td>ID</td>
          <td>Type of events</td>
          <td>Date of events</td>
          <td>Time of events</td>
          <td>Number of visitor</td>
          <td>Unit No</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($events as $event)
        <tr>
            <td>{{$event->id}}</td>
            <td>{{$event->type_event}}</td>
            <td>{{$event->select_date}}</td>
            <td>{{$event->select_time}}</td>
            <td>{{$event->num_visitor}}</td>
            <td>{{$event->unitNo}}</td>
            <td>
                <a href="{{ route('events.edit',$event->id)}}" button class="btn btn-primary"><i class="fa fa-pencil"></i></a>
            </td>
            <td>
                <form action="{{ route('events.destroy', $event->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" onclick="return confirm('Are you sure to delete?')"><i class="fa fa-trash-o"></i></button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
</div>
</div>
@endsection

<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div>
  @endif
