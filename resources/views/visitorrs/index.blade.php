@extends('layouts.app')

@section('content')
  <div class="container">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
      <div class="row">
  <div class="col-sm-12">
      <h1 class="display-3">Visitors</h1>
      <table class="table table-striped">

        <div class="col-md-5">
          <form action="/search2" method="get">
          <div class="input-group">
            <button type="submit"a href="http://localhost:8000/visitorrs"><i class="fa fa-arrow-left" style="font-size:15px;color:black" ></i></a></button>
            <input type="search2" name="search2" class="form-control">
            <span class="input-group-prepend">
              <button type="submit" class="btn btn-primary">Search</button>
            </span>
          </div>
          </form>
        </div>

        <br>
      <thead>
          <tr>
            <td>No</td>
            <td>Full Name</td>
            <td>IC Number</td>
            <td>Email</td>
            <td>Phone Number</td>
            <td>Type of vehicle</td>
            <td>Date</td>
            <td>Plate Number</td>
            <td>Status</td>
            <td colspan = 2>Actions</td>
          </tr>
      </thead>
      <tbody>
          @foreach($visitorrs as $visitorr)
          <tr>
              <td>{{$loop->iteration}}</td>
              <td>{{$visitorr->full_name}}</td>
              <td>{{$visitorr->ic_no}}</td>
              <td>{{$visitorr->email}}</td>
              <td>{{$visitorr->phone_number}}</td>
              <td>{{$visitorr->type_vehicle}}</td>
              <td>{{$visitorr->date}}</td>
              <td>{{$visitorr->plate_number}}</td>
              <td>{{$visitorr->status== "CheckIn" ? 'Check In' : 'Pending'}}</td>
              <td>
                  <a href="{{ route('visitorrs.edit',$visitorr->id)}}" button class="btn btn-primary"><i class="fa fa-pencil"></i></button></a>
              </td>
              <td>
                  <form action="{{ route('visitorrs.destroy', $visitorr->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger" onclick="return confirm('Are you sure to delete?')"><i class="fa fa-trash-o"></i></button>
                  </form>
              </td>
          </tr>
          @endforeach
      </tbody>
    </table>
  <div>
  </div>
  </div>
  @endsection

  <div class="col-sm-12">

    @if(session()->get('success'))
      <div class="alert alert-success">
        {{ session()->get('success') }}
      </div>
    @endif
