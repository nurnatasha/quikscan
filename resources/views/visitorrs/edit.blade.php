@extends('layouts.app')

@section('content')
<div class="container">
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update a visitor</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br />
        @endif
        <form method="post" action="{{ route('visitorrs.update', $visitorr->id) }}">
            @method('PATCH')
            @csrf
            <div class="form-group">

                <label for="full_name">Full Name:</label>
                <input type="text" class="form-control" name="full_name" value={{ $visitorr->full_name }} />
            </div>

            <div class="form-group">
                <label for="ic_no">IC NO:</label>
                <input type="text" class="form-control" name="ic_no" value={{ $visitorr->ic_no }} />
            </div>

            <div class="form-group">
                <label for="email">Email:</label>
                <input type="text" class="form-control" name="email" value={{ $visitorr->email }} />
            </div>
            <div class="form-group">
                <label for="phone_number">Phone Number:</label>
                <input type="text" class="form-control" name="phone_number" value={{ $visitorr->phone_number }} />
            </div>
            <div class="form-group">
                <label for="type_vehicle">Type of vehicle:</label>
                <input type="text" class="form-control" name="type_vehicle" value={{ $visitorr->type_vehicle }} />
            </div>
            <div class="form-group">
                <label for="color">Color:</label>
                <input type="text" class="form-control" name="color" value={{ $visitorr->color }} />
            </div>
			      <div class="form-group">
                <label for="plate_number">Plate Number:</label>
                <input type="text" class="form-control" name="plate_number" value={{ $visitorr->plate_number }} />
            </div>
          


            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
@endsection
