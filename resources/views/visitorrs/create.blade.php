@extends('layouts.app')

@section('content')
<head>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
</head>
<style>
.col-20 {
float: left;
width: 20%;
margin-top: 6px;
}

.col-75 {
float: left;
width: 75%;
margin-top: 6px;
}
</style>

<div class="container">
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3"><center>Register Visitor</center></h1>
  <div>

      <form method="post" action="{{ route('visitorrs.store') }}">
          @csrf
          <div class="col-20">
              <label for="full_name">Visitor Name*</label>
          </div>
          <div class="col-75">
              <input type="text" class="form-control" name="full_name"/>
          </div>

          <br>
          </br>

          <div class="col-20">
              <label for="ic_no">IC Number*</label>
          </div>
          <div class="col-75">
              <input type="text" class="form-control" name="ic_no"/>
          </div>

          <br>
          </br>

          <div class="col-20">
              <label for="email">Email</label>
          </div>
          <div class="col-75">
              <input type="text" class="form-control" name="email"/>
          </div>

          <br>
          </br>

          <div class="col-20">
              <label for="phone_number">Phone Number</label>
          </div>
          <div class="col-75">
              <input type="text" class="form-control" name="phone_number"/>
          </div>

          <br>
          </br>

          <div class="col-20">
              <label for="date">Date*</label>
          </div>
          <div class="col-75">
              <input type="text" class="date form-control" name="date" />
          </div>

          <script type="text/javascript">
            $('.date').datepicker({
                format: 'dd/mm/yyyy'
                });
          </script>


          <br>
          </br>

          <div class="col-20">
            <label for="type_vehicle">Type of vehicle*</label>
          </div>
          <div class="col-75">
            <select id="type_vehicle" class="form-control" name="type_vehicle">
                <option value="car">Car</option>
                <option value="motorcyle">Motorcyle</option>
                <option value="lorry">Lorry</option>
                <option value="van">Van</option>
            </select>
          </div>

          <br>
          </br>

	        <div class="col-20">
              <label for="plate_number">Plate Number*</label>
          </div>
          <div class="col-75">
              <input type="text" class="form-control" name="plate_number"/>
          </div>



          <br>
          <br>
          </br>
          </br>

          <center>
          <button type="submit" class="btn btn-success">Register</button>
          </center>

      </form>
  </div>
</div>
</div>
</div>
@endsection
