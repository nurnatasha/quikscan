@extends('layouts.appAdmin3')

@section('content')
  <div class="container">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
      <div class="row">
  <div class="col-sm-12">
      <h1 class="display-3">Visitors</h1>
      <table class="table table-striped">

        <div class="col-md-5">
          <form action="/search7" method="get">
          <div class="input-group">
            <button type="submit"a href="http://localhost:8000/historyVisitor"><i class="fa fa-arrow-left" style="font-size:15px;color:black" ></i></a></button>
            <input type="search7" name="search7" class="form-control">
            <span class="input-group-prepend">
              <button type="submit" class="btn btn-primary">Search</button>
            </span>
          </div>
          </form>
        </div>

        <br>
      <thead>
          <tr>
            <td>No</td>
            <td>Full Name</td>
            <td>IC Number</td>
            <td>Email</td>
            <td>Phone Number</td>
            <td>Date</td>
            <td>Type of vehicle</td>
            <td>Plate Number</td>
            <td>Unit No</td>
            <td colspan = 2>Status</td>
          </tr>
      </thead>
      <tbody>
          @foreach($visitorrs as $visitorr)
          <tr>
              <td>{{$loop->iteration}}</td>
              <td>{{$visitorr->full_name}}</td>
              <td>{{$visitorr->ic_no}}</td>
              <td>{{$visitorr->email}}</td>
              <td>{{$visitorr->phone_number}}</td>
              <td>{{$visitorr->date}}</td>
              <td>{{$visitorr->type_vehicle}}</td>
              <td>{{$visitorr->plate_number}}</td>
              <td>{{$visitorr->unitNo}}</td>
              <td>{{$visitorr->status}}</td>
              <td>{{$visitorr->status2}}</td>

          </tr>
          @endforeach
      </tbody>
    </table>
  <div>
  </div>
  </div>
  @endsection

  <div class="col-sm-12">

    @if(session()->get('success'))
      <div class="alert alert-success">
        {{ session()->get('success') }}
      </div>
    @endif
