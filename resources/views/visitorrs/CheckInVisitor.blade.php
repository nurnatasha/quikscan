
  @extends('layouts.appAdmin')

  @section('content')
  <div class="container">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>

<style>
.preview{
   width:200px;
   height: 200px;
   margin:0px auto;

}
</style>

    <video id="preview"></video>

    <div class="row">
    <div class="col-sm-15"><a class="nav-link" href="http://localhost:8000/CheckInVisitor"><b>Visitors</b></a>
    </div>

    <div class="col-md-5">
      <form action="/search" method="get">
      <div class="input-group">
        <input type="search" name="search" class="form-control">
        <span class="input-group-prepend">
          <button type="submit" class="btn btn-primary">Search</button>
        </span>
      </div>
      </form>
    </div>

    <script type="text/javascript">
        var scanner = new Instascan.Scanner({ video: document.getElementById('preview'), scanPeriod: 5, mirror: false });
        scanner.addListener('scan',function(content){
            //alert(content);
            window.location.href=content;
        });
        Instascan.Camera.getCameras().then(function (cameras){
            if(cameras.length>0){
                scanner.start(cameras[0]);
                $('[name="options"]').on('change',function(){
                    if($(this).val()==1){
                        if(cameras[0]!=""){
                            scanner.start(cameras[0]);
                        }else{
                            alert('No Front camera found!');
                        }
                    }else if($(this).val()==2){
                        if(cameras[1]!=""){
                            scanner.start(cameras[1]);
                        }else{
                            alert('No Back camera found!');
                        }
                    }
                });
            }else{
                console.error('No cameras found.');
                alert('No cameras found.');
            }
        }).catch(function(e){
            console.error(e);
            alert(e);
        });
    </script>


    <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Full Name</td>
          <td>IC No</td>
          <td>Phone Number</td>
          <td>Plate Number</td>
          <td>Unit No</td>
          <td>Status</td>


          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($visitorrs as $visitorr)
          <tr>
            <td>{{$visitorr->id}}</td>
            <td>{{$visitorr->full_name}}</td>
            <td>{{$visitorr->ic_no}}</td>
            <td>{{$visitorr->phone_number}}</td>
            <td>{{$visitorr->plate_number}}</td>
            <td>{{$visitorr->unitNo}}</td>
            <td>{{$visitorr->status =="CheckIn" ? 'Check In' : 'Pending'}}</td>
            <td>
              @if ($visitorr->status=="CheckIn")
                  <a href="{{ route('inactive', $visitorr->id)}}" class="fa fa-arrow-up"</a>
              @else
                  <a href="{{ route('active', $visitorr->id)}}" class="fa fa-arrow-down"</a>
              @endif
            </td>
          </tr>
        @endforeach
    </tbody>
    </table>
    </div>
  </div>
  @endsection
