@extends('layouts.app')

@section('content')
<div class="container">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <div class="row">
<div class="col-sm-12">
    <h3 class="display-3">Pre-register List</h3>
    <table class="table table-striped">

      <div class="col-md-5">
        <form action="/search6" method="get">
        <div class="input-group">
          <button type="submit"a href="http://localhost:8000/reg_visitors"><i class="fa fa-arrow-left" style="font-size:15px;color:black" ></i></a></button>
          <input type="search6" name="search6" class="form-control">
          <span class="input-group-prepend">
            <button type="submit" class="btn btn-primary">Search</button>
          </span>
        </div>
        </form>
      </div>

      <br>

    <thead>
        <tr>
          <td>No</td>
          <td>Full Name</td>
          <td>Phone Number</td>
          <td>Plate Number</td>
          <td>Unit No</td>
          <td>Date</td>
          <td colspan = 2>Status</td>
        </tr>
    </thead>
    <tbody>
      @foreach($reg_visitors as $reg_visitor)

        <td>  {{$loop->iteration}}</td>
        <td>  {{$reg_visitor->full_name}} </td>
        <td>  {{$reg_visitor->phone_number}} </td>
        <td>  {{$reg_visitor->plate_number}} </td>
        <td>  {{$reg_visitor->unitNo}} </td>
        <td>  {{$reg_visitor->date}} </td>
        <td>  {{$reg_visitor->status3}} </td>
        <td>  {{$reg_visitor->status4}} </td>

        </tr>
        @endforeach
    </tbody>
  </table>
<div>
</div>
</div>
@endsection


<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div>
  @endif
