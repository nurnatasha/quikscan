@extends('layouts.appAdmin')

@section('content')
<div class="container">
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update a visitor</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br />
        @endif
        <form method="post" action="{{ route('reg_visitors.update', $reg_visitor->id) }}">
            @method('PATCH')
            @csrf
            <div class="form-group">

                <label for="full_name">Full Name:</label>
                <input type="text" class="form-control" name="full_name" value={{ $reg_visitor->full_name }} />
            </div>

            <div class="form-group">
                <label for="ic_no">IC NO:</label>
                <input type="text" class="form-control" name="ic_no" value={{ $reg_visitor->ic_no }} />
            </div>
            <div class="form-group">
                <label for="phone_number">Phone Number:</label>
                <input type="text" class="form-control" name="phone_number" value={{ $reg_visitor->phone_number }} />
            </div>
            <div class="form-group">
                <label for="plate_number">Plate Number:</label>
                <input type="text" class="form-control" name="plate_number" value={{ $reg_visitor->plate_number }} />
            </div>
            <div class="form-group">
                <label for="unitNo">Unit No:</label>
                <input type="text" class="form-control" name="unitNo" value={{ $reg_visitor->unitNo }} />
            </div>

            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
@endsection
