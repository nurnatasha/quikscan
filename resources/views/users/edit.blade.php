@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
      <div class="col-sm-8 offset-sm-2">
          <h1 class="display-3">Edit Profile</h1>

          @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
          <br />
          @endif
          <form method="post" action="{{ route('users.update', $users->id) }}">
            @method('PATCH')
              @csrf
              <div class="form-group">

                  <label for="name">Full Names:</label>
                  <input type="text" class="form-control" name="name" value= "{{ Auth::user()->name }}"/>
              </div>
              <div class="form-group">
                  <label for="email">Email:</label>
                  <input type="text" class="form-control" name="email" value="{{ Auth::user()->email }}"/>
              </div>
              <div class="form-group">
                  <label for="phoneNo">Phone No:</label>
                  <input type="text" class="form-control" name="phoneNo" value="{{ Auth::user()->phoneNo }}"/>
              </div>

              <button type="submit" class="btn btn-primary">Update</button>
          </form>
      </div>
</div>
@endsection
