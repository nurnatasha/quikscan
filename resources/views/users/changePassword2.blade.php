@extends('layouts.appAdmin')

@section('content')
<div class="container">
  <div class="row">
      <div class="col-sm-8 offset-sm-2">
          <h1 class="display-3">Change Password</h1>

          @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
          <br />
          @endif
            <form method="post" action="{{ url('/users/updatePassword2') }}">

              @csrf
              <div class="form-group">
                  <label for="old_password">Old Password:</label>
                  <input type="password" class="form-control" name="old_password" />
              </div>
              <div class="form-group">
                  <label for="password">New Password:</label>
                  <input type="password" class="form-control" name="new_password" />
              </div>
              <div class="form-group">
                  <label for="confirm_password">New Password:</label>
                  <input type="password" class="form-control" name="confirm_password" />
              </div>

              <button type="submit" class="btn btn-primary">Update</button>
          </form>
      </div>
</div>
@endsection
