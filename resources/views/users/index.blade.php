@extends('layouts.app')

@section('content')
@if(\Session::has('success'))
    <div class="alert alert-success" role="alert">
        <p>{{ Session::get('success') }}</p>
    </div>
@endif
@if(\Session::has('error'))
    <div class="alert" role="alert">
        <p>{{ Session::get('error') }}</p>
    </div>
@endif
<div class="container">
  <div class="row">
      <div class="col-sm-8 offset-sm-2">
          <h1 class="display-3">Profile</h1>

              <div class="form-group">

                  <label for="full_name">Full Name: </label>
                  {{ Auth::user()->name }}

              </div>
              <div class="form-group">
                  <label for="email">Email: </label>
                   {{ Auth::user()->email }}
              </div>
              <div class="form-group">
                  <label for="user_id">Unit No: </label>
                  {{ Auth::user()->user_id }}
              </div>
              <div class="form-group">
                  <label for="phoneNo">Phone No: </label>
                  {{ Auth::user()->phoneNo }}
              </div>

              <a href="{{ route('users.edit', $users->user_id = Auth::user()->user_id)}}" class="btn btn-primary btn-sm">Edit</a>
              <a href="{{ url('/changePassword2')}}" class="btn btn-primary btn-sm">Change Password</a>
          </form>
      </div>
</div>
@endsection
