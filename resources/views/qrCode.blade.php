@extends('layouts.app')

@section('content')
<div class="container">
<!DOCTYPE html>

<html>

<body>



<div class="visible-print text-center">

    <h1>Send this QR Code to the visitor</h1>


  {!! QrCode::size(250)->generate('http://localhost:8000/displayCheckIn/'.$qrcode->token); !!}
  <br>
  <br>
  {!! QrCode::size(250)->generate('http://localhost:8000/displayCheckOut/'.$qrcode->token); !!}

     	<br>
	<br>
 	<center>
  <!--<input type="text" value="{{ Auth::user()->name }}" id="myInput">-->
    <button onclick="myFunction()">Copy</button>
    <script>
      function myFunction() {
        var copyText = document.getElementById("myInput");
        copyText.select();
        copyText.setSelectionRange(0, 99999)
        document.execCommand("copy");
        alert("Copied the text: " + copyText.value);
        }
    </script>
    <br>
    <br>
      <a href="http://localhost:8000/displayCheckIn/{{ $qrcode->token}}">Link to check in</a>
      <br>
      <a href="http://localhost:8000/displayCheckOut/{{ $qrcode->token}}">Link to check out</a>
      <br>
    	<a href="{{ url('/visitorrs') }}">Home Page</a>
	</center>
	</br>
	</br>

</div>

</body>

</html>

</div>

@endsection
