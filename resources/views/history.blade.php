@extends('layouts.app')

@section('content')
<div class="container">
  <style>
  .button {
  background-color:#66b8b2; /* Green */
  border: none;
  color: white;
  padding: 60px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 20px;
  margin: 4px 2px;
  cursor: pointer;
  }
  .button1 {border-radius: 12%;}
  </style>

    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3"><center>History</center></h1>

    <br>
    </br>

    <center>
    <a href="{{ url('/visitorrs') }}" button class="button button1">Visitors</button></a>

    <a href="{{ url('/events') }}" button class="button button1">Events</button></a>
    <br>
    <a href="{{ url('/reg_visitors') }}" button class="button button1">Pre Register</button></a>
    </center>
    </div>
</div>
@endsection
