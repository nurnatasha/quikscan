<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Visitorr;
use DB;

class CheckInController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $visitorrs= Visitorr::Latest('id')->get();
		    return view('visitorrs.CheckInVisitor', compact('visitorrs'));
    }

	public function adminHome()
    {
        return view('adminHome');
    }

    public function search(Request $request)
    {
        $search= $request->get('search');
        $visitorrs=DB::table('visitorrs')->where('full_name', 'like', '%'.$search.'%')->paginate(5);
		    return view('visitorrs.CheckInVisitor', compact('visitorrs'));
    }

      /**public function edit2($id)
    {
        $visitorr = Visitorr::find($id);
        return view('visitorrs.edit2', compact('visitorrs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response

    public function update2(Request $request, $id)
    {
        $request->validate([
            'full_name'=>'required',
            'ic_no'=>'required',
            'phone_number'=>'required',
			      'type_vehicle'=>'required',
			      'color'=>'required',
			      'plate_number'=>'required',
            'status'=>'required'
        ]);

        $visitorr = Visitorr::find($id);
        $visitorr->full_name =  $request->get('full_name');
        $visitorr->ic_no = $request->get('ic_no');
        $visitorr->email = $request->get('email');
        $visitorr->phone_number = $request->get('phone_number');
        $visitorr->type_vehicle = $request->get('type_vehicle');
        $visitorr->color = $request->get('color');
		    $visitorr->plate_number = $request->get('plate_number');
        $visitorr->status = $request->get('status');
        $visitorr->save();

        return redirect('/CheckInVisitor')->with('success', 'Visitor updated!!!');

    }
    */
    public function inactive($id)
    {
      $visitorr = Visitorr::find($id);
      $visitorr->status ="CheckIn";
      $visitorr->save();

      return redirect('/CheckInVisitor')->with('success', 'Visitor updated!!!');
    }

    public function active($id)
    {
      $visitorr = Visitorr::find($id);
      $visitorr->status = "CheckIn";
      $visitorr->save();

      return redirect('/CheckInVisitor')->with('success', 'Visitor updated!!!');
    }


}
