<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Visitorr;
use DB;
class HistoryVisitorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $visitorrs= Visitorr::Latest('id')->get();
		    return view('visitorrs.historyVisitor', compact('visitorrs'));
    }

	  public function adminHome()
    {
        return view('adminHome');
    }

    public function search7(Request $request)
    {
        $search7= $request->get('search7');
        $visitorrs=DB::table('visitorrs')->where('full_name', 'like', '%'.$search7.'%')->paginate(9);
		     return view('visitorrs.historyVisitor', compact('visitorrs'));
    }
  





}
