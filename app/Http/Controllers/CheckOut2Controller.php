<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RegVisitor;
use DB;

class CheckOut2Controller extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index2()
    {
        $reg_visitors= RegVisitor::Latest('id')->get();
		    return view('reg_visitors.CheckOut', compact('reg_visitors'));
    }

	public function adminHome()
    {
        return view('adminHome');
    }

    public function search9(Request $request)
    {
        $search= $request->get('search9');
        $reg_visitors=DB::table('reg_visitors')->where('full_name', 'like', '%'.$search.'%')->paginate(5);
		    return view('reg_visitors.CheckOut', compact('reg_visitors'));
    }

    public function inactive4($id)
    {
      $reg_visitor = RegVisitor::find($id);
      $reg_visitor->status4 = "CheckOut";
      $reg_visitor->save();

      return redirect('/CheckOut')->with('success', 'Visitor updated!!!');
    }

    public function active4($id)
    {
      $reg_visitor = RegVisitor::find($id);
      $reg_visitor->status4 = "CheckOut";
      $reg_visitor->save();

      return redirect('/CheckOut')->with('success', 'Visitor updated!!!');
    }



}
