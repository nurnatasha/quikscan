<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Visitorr;
use App\User;
use DB;
use Auth;
use Illuminate\Support\Str;
use Carbon\Carbon;

class VisitorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      //$users=Auth::user()->user_id;
      //$visitorrs= Visitorr::Latest('id')->get();
      //$visitorrs= Visitorr::with('users')->where('user_id','3')->get();
		  //  return view('visitorrs.index', compact('visitorrs'));
      $visitorrs= Visitorr::all()->where('unitNo', Auth::user()->user_id);
      return view('visitorrs.index', compact('visitorrs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create(Visitorr $visitorrs)
     {
         $visitorrs->user_id = Auth::id();
         return view('visitorrs.create');
     }

    public function checkin($token)
    {
      $checkin = DB::table('qrcode')->where('token', $token)->first();
      $visitors = DB::table('visitorrs')->where('id', $checkin->id)->first();
      return view('visitorrs.displayCheckIn', compact('visitors', 'checkin'));
    }

    public function checkedin(Request $request){
        $pelawat = DB::table('qrcode')->where('token', $request->get('token'))->first();
        date_default_timezone_set('Asia/Kuala_Lumpur');
        $visitorr = DB::table('visitorrs')->where('id', $pelawat->id)->update(['status' => "CheckIn", 'timeCheckin' => date("H:i:s")]);
        //DB::table('qrcode')->where('token', $request->get('token'))->delete();
        return redirect('/CheckInVisitor')->with('success', 'Visitor checked in!');
    }

    public function checkout($token)
    {
      $checkout = DB::table('qrcode')->where('token', $token)->first();
      $visitors = DB::table('visitorrs')->where('id', $checkout->id)->first();
      return view('visitorrs.displayCheckOut', compact('visitors', 'checkout'));
    }

    public function checkedout(Request $request)
    {
        $pelawat = DB::table('qrcode')->where('token', $request->get('token'))->first();
        date_default_timezone_set('Asia/Kuala_Lumpur');
        $visitorr = DB::table('visitorrs')->where('id', $pelawat->id)->update(['status2' => "CheckOut", 'timeCheckout' => date("H:i:s")]);
        DB::table('qrcode')->where('token', $request->get('token'))->delete();
        return redirect('/CheckOutVisitor')->with('success', 'Visitor checked out!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $visitorr = new Visitorr([
            'full_name' => $request->get('full_name'),
            'ic_no' => $request->get('ic_no'),
            'email' => $request->get('email'),
            'phone_number' => $request->get('phone_number'),
            'date' => $request->get('date'),
            'type_vehicle' => $request->get('type_vehicle'),
	          'plate_number' => $request->get('plate_number'),
            'unitNo' => Auth::user()->user_id,
            'user_id' => Auth::user()->id

        ]);
        $visitorr->save();

        $pelawat = DB::table('visitorrs')->orderBy('id', 'DESC')->first();
        $token = Str::random(60);
        DB::table('qrcode')->insert(
          ['id' => $pelawat->id, 'token' => $token, 'created_at' => Carbon::now()]
        );

        $qrcode = DB::table('qrcode')->where('id', $pelawat->id)->first();
        return view('qrCode', compact('qrcode'));


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $visitorr = Visitorr::find($id);
        return view('visitorrs.edit', compact('visitorr'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $visitorr = Visitorr::find($id);
        $visitorr->full_name =  $request->get('full_name');
        $visitorr->ic_no = $request->get('ic_no');
        $visitorr->email = $request->get('email');
        $visitorr->phone_number = $request->get('phone_number');
        $visitorr->date = $request->get('date');
        $visitorr->type_vehicle = $request->get('type_vehicle');
		    $visitorr->plate_number = $request->get('plate_number');
        $visitorr->unitNo = Auth::user()->user_id;
        $visitorr->user_id =Auth::user()->id;
        $visitorr->save();

        return redirect('/visitorrs')->with('success', 'Visitor updated!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $visitorr = Visitorr::find($id);
        $visitorr->delete();

        return redirect('/visitorrs')->with('success', 'Visitor deleted!');

    }



    public function search2(Request $request)
    {
        $search= $request->get('search2');
        $visitorrs=DB::table('visitorrs')->where('full_name', 'like', '%'.$search.'%')->paginate(5);
        return view('visitorrs.index', compact('visitorrs'));

    }





}
