<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Visitorr;
use App\RegVisitor;
use DB;

class CheckOutController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $visitorrs= Visitorr::Latest('id')->get();
		    return view('visitorrs.CheckOutVisitor', compact('visitorrs'));
    }
    
	public function adminHome()
    {
        return view('adminHome');
    }

    public function search4(Request $request)
    {
        $search= $request->get('search4');
        $visitorrs=DB::table('visitorrs')->where('full_name', 'like', '%'.$search.'%')->paginate(5);
		    return view('visitorrs.CheckOutVisitor', compact('visitorrs'));
    }

    public function inactive2($id)
    {
      $visitorr = Visitorr::find($id);
      $visitorr->status2 = "CheckOut";
      $visitorr->save();

      return redirect('/CheckOutVisitor')->with('success', 'Visitor updated!!!');
    }

    public function active2($id)
    {
      $visitorr = Visitorr::find($id);
      $visitorr->status2 = "CheckOut";
      $visitorr->save();

      return redirect('/CheckOutVisitor')->with('success', 'Visitor updated!!!');
    }


}
