<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use DB;
class HistoryEventController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      $events= Event::Latest('id')->get();
      return view('events.historyEvent', compact('events'));
    }

	  public function adminHome()
    {
        return view('adminHome');
    }

    public function search8(Request $request)
    {
        $search8= $request->get('search7');
        $events=DB::table('events')->where('full_name', 'like', '%'.$search7.'%')->paginate(9);
		     return view('events.historyEvent', compact('events'));
    }





}
