<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use DB;
use Auth;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$events= Event::with('users')->where('user_id','1')->get();
		    //return view('events.index', compact('events'));
        $events= Event::with('users')->where('user_id',Auth::user()->id)->get();
        return view('events.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Event $events)
    {
      $events->user_id = Auth::id();
        return view('events.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'type_event'=>'required',
            'select_date'=>'required',
            'select_time'=>'required',
 	          'num_visitor'=>'required',

        ]);

        $event = new Event([
            'type_event' => $request->get('type_event'),
            'select_date' => $request->get('select_date'),
            'select_time' => $request->get('select_time'),
            'num_visitor' => $request->get('num_visitor'),
            'unitNo' => Auth::user()->user_id,
            'user_id' => Auth::user()->id
        ]);
        $event->save();
        return redirect('/events')->with('success', 'Event saved!');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::find($id);
        return view('events.edit', compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'type_event'=>'required',
            'select_date'=>'required',
            'select_time'=>'required',
	          'num_visitor'=>'required',

        ]);

        $event = Event::find($id);
        $event->type_event =  $request->get('type_event');
        $event->select_date = $request->get('select_date');
        $event->select_time = $request->get('select_time');
        $event->num_visitor = $request->get('num_visitor');
        $event->unitNo = Auth::user()->user_id;
        $event->user_id = Auth::user()->id;

        $event->save();

        return redirect('/events')->with('success', 'Event updated!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::find($id);
        $event->delete();

        return redirect('/events')->with('success', 'Event deleted!');

    }

    public function search3(Request $request)
    {
        $search= $request->get('search3');
        $events=DB::table('events')->where('type_event', 'like', '%'.$search.'%')->paginate(5);
        return view('events.index', compact('events'));
    }
}
