<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RegVisitor;
use DB;

class historyPreRegisterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      $reg_visitors= RegVisitor::Latest('id')->get();
      return view('reg_visitors.historyPreRegister', compact('reg_visitors'));
    }



}
