<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Hash;

class UserController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
  public function index(Request $request)
  {
    $users= User::with('users')->where('user_id',Auth::user()->id)->get();
    return view('users.index', compact('users'));
  }

    public function edit()
  {

        $users = User::where('id','=', Auth ::user()->id)->first();
        //return view('users.edit', compact('users'));
        return view('users.edit', compact('users'));
  }

  public function update(Request $request,$id)
    {

        $users = User::find($id);
        $users->name =  $request->get('name');
        $users->email = $request->get('email');
        $users->phoneNo = $request->get('phoneNo');

        $users->save();

        return redirect('/userEdit')->with('success', 'User updated!');

    }

    public function adminIndex(Request $request)
    {
      $users= User::with('users')->where('user_id',Auth::user()->id)->get();
      return view('users.adminIndex', compact('users'));
    }

      public function edit2()
    {

          $users = User::where('id','=', Auth ::user()->id)->first();
          return view('users.edit2', compact('users'));
    }

    public function update2(Request $request,$id)
      {

          $users = User::find($id);
          $users->name =  $request->get('name');
          $users->email = $request->get('email');

          $users->save();

          return redirect('/adminProfile')->with('success', 'User updated!');

      }

      public function changePassword()
      {
          $users = User::where('id','=', Auth ::user()->id)->first();
          return view('users.changePassword',compact('users'));
      }

      public function updatePassword(Request $request)
      {
        if(!(Hash::check($request->get('old_password'),Auth::user()->password)))
        {
            return back()->with('error', 'Your old password does not match with what you provided');
        }
        if(strcmp($request->get('old_password'), $request->get('new_password')) == 0)
        {
          return back()->with('error', 'Your new password cannot be same with the current password');
        }
        if(strcmp($request->get('confirm_password'), $request->get('new_password')) != 0)
        {
          return back()->with('error', 'Your new password does not match with the confirm password');
        }
        $request->validate([
            'old_password' => 'required',
            'new_password' => 'required|string|min:8',
        ]);
        //save the Password update
        $user = Auth::user();
        $user->password = bcrypt($request->get('new_password'));
        $user->save();
        return redirect('/adminProfile')->with('success', 'Password successfully changed. An email has been sent.');
      }

      public function changePassword2()
      {
          $users = User::where('id','=', Auth ::user()->id)->first();
          return view('users.changePassword2',compact('users'));
      }

      public function updatePassword2(Request $request)
      {
        if(!(Hash::check($request->get('old_password'),Auth::user()->password)))
        {
            return back()->with('error', 'Your old password does not match with what you provided');
        }
        if(strcmp($request->get('old_password'), $request->get('new_password')) == 0)
        {
          return back()->with('error', 'Your new password cannot be same with the current password');
        }
        if(strcmp($request->get('confirm_password'), $request->get('new_password')) != 0)
        {
          return back()->with('error', 'Your new password does not match with the confirm password');
        }
        $request->validate([
            'old_password' => 'required',
            'new_password' => 'required|string|min:8',
        ]);
        //save the Password update
        $user = Auth::user();
        $user->password = bcrypt($request->get('new_password'));
        $user->save();
        return redirect('/userEdit')->with('success', 'Password successfully changed. An email has been sent.');
      }

  }
