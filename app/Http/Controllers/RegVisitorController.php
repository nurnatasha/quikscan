<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RegVisitor;
use DB;
use Auth;
use App\User;

class RegVisitorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //$reg_visitors= RegVisitor::Latest('id')->get();
		    //return view('reg_visitors.index', compact('reg_visitors'));
        $reg_visitors= RegVisitor::all()->where('unitNo', Auth::user()->user_id);
        return view('reg_visitors.index', compact('reg_visitors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $unitno = DB::table('users')->orderBy('user_id')->get();
        return view('reg_visitors.create', compact('unitno'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


            $reg_visitor = new RegVisitor([
            'full_name' => $request->get('full_name'),
            'ic_no' => $request->get('ic_no'),
            'phone_number' => $request->get('phone_number'),
	          'plate_number' => $request->get('plate_number'),
            'unitNo' => $request->get('unitNo'),
            'date' => $request->get('date'),
            'select_date' => $request->get('select_date'),

        ]);
        $reg_visitor->save();
        return redirect('/CheckInVisitor')->with('success', 'Contact saved!');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reg_visitor = RegVisitor::find($id);
        return view('reg_visitors.edit', compact('reg_visitor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $reg_visitor = RegVisitor::find($id);
        $reg_visitor->full_name =  $request->get('full_name');
        $reg_visitor->ic_no = $request->get('ic_no');
        $reg_visitor->phone_number = $request->get('phone_number');
		    $reg_visitor->plate_number = $request->get('plate_number');
        $reg_visitor->unitNo = $request->get('unitNo');
        $reg_visitor->save();

        return redirect('/reg_visitors')->with('success', 'Visitor updated!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reg_visitor = RegVisitor::find($id);
        $reg_visitor->delete();

        return redirect('/reg_visitors')->with('success', 'Visitor deleted!');

    }

    public function search6(Request $request)
    {
        $search6= $request->get('search6');
        $reg_visitors=DB::table('reg_visitors')->where('full_name', 'like', '%'.$search6.'%')->paginate(9);
		     return view('reg_visitors.index', compact('reg_visitors'));
    }

}
