<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Visitorr extends Model
{
    protected $fillable =
	[
  'visitorrs_id',
	'full_name',
	'ic_no',
	'email',
	'phone_number',
	'type_vehicle',
	'plate_number',
  'status',
  'status2',
  'unitNo',
  'date',
  'user_id',

	];

  //public function users()
  //{
  //  return $this->belongsTo ('App\User');
  //}

  public function users()
  {

          return $this->belongsTo(User::class);
  }

}
