<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable =
	[
	'type_event',
	'select_date',
	'select_time',
	'num_visitor',
  'unitNo',
  'user_id',
	];

  public function users()
  {

          return $this->belongsTo(User::class);
  }
}
