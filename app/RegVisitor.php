<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegVisitor extends Model
{
    protected $fillable =
	[
	'full_name',
	'ic_no',
	'phone_number',
	'plate_number',
  'unitNo',
  'date',
  'status3',
  'status4'
	];

  public function users()
  {

          return $this->belongsTo(User::class);
  }

}
