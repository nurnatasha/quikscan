<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitorrsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitorrs', function (Blueprint $table) {
      $table->id();
      $table->timestamps();
      $table->string('visitorrs_id');
      $table->string('user_id');
			$table->string('full_name');
			$table->string('ic_no');
			$table->string('email');
			$table->string('phone_number');
      $table->string('date');
			$table->string('type_vehicle');
			$table->string('plate_number');
      $table->boolean('status');
      $table->boolean('status2');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitorrs');
    }
}
