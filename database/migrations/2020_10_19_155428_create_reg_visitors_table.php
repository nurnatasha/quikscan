<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegVisitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reg_visitors', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
	    $table->string('full_name');
			$table->string('ic_no');
			$table->string('email');
			$table->string('phone_number');
			$table->string('type_vehicle');
			$table->string('color');
			$table->string('plate_number');
      $table->string('status3');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reg_visitors');
    }
}
